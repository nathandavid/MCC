Participants = new Meteor.Collection('participants');

Participants.allow({
	insert: function (firstName, lastName, league, partner) {
    	return true;
	},

	remove: function () {
		return true;
	},

	update: function () {
		return true;
	}
});