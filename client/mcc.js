Meteor.subscribe("participants");

if (Meteor.isClient) {
  Template.participants.juniors = function () {
    return Participants.find({league: "Junior"});
  };

  Template.participants.seniors = function () {
    return Participants.find({league: "Senior"});
  };

  Template.signup.events({
    'click button' : function () {
    	if (firstName.value && lastName.value) {
    		Participants.insert({
        		firstName: firstName.value,
        		lastName: lastName.value,
        		league: league.value,
            partner: partner.value
    		});
    	} else {
    		console.log('Form not complete.');
    	}; 
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}